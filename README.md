# Multiplicative Integer Group #

This Go package implements multiplicative integer group for usage with
`gospake2` package. Package implements the `Group` interface defined in
`gospake2` package. This package is written by referencing *groups.py* from
*python-spake2* project.

## Number Groups ##

A cyclic abelian group, in mathematical sense, is a collection of *elements* and
a (binary) operation that takes two element and produces a third. A group should
have following properties

* There is an *identity* element such that X+Identity = X
* There is a generator element G
* Adding G `n` times is called scalar multiplication: Y=n*G
* Addition loops around after *q* times called *order* of subgroup
  This means (n+k*q)*X = n*X
* scalar multiplication is associative n*(X+Y) = n*X + n*Y
* *scalar division* is multiplying by *q-n`*

A *scalar* is an integer in [0,q-1] inclusive. Scalars can be added to each
other, multiplied or inverted. You can go from scalar to Element of group but
its hard (in cryptographic sense) to do the reverse.

# License #
Copyright: 2018, Vasudev Kamath <vasudev@copyninja.info>

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Library General Public License for more details.

You should have received a copy of the GNU Library General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 51
Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

