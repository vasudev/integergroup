package integergroup

import (
	"math/big"
	group "salsa.debian.org/vasudev/gospake2/groups"
)

type IntegerElement struct {
	params *GroupParameters
	e      *big.Int
}

func (i IntegerElement) Add(other group.Element) group.Element {
	a := i.e
	b := other.(IntegerElement).e

	if !i.params.equal(other.(IntegerElement).params) {
		panic("You can't add elements of 2 different groups")
	}

	result := new(big.Int).Mul(a, b)

	return group.Element(IntegerElement{params: i.params, e: result.Mod(result, i.params.p)})
}

func (i IntegerElement) ScalarMult(s *big.Int) group.Element {
	reducedS := new(big.Int).Mod(s, i.params.q)
	return group.Element(IntegerElement{params: i.params, e: new(big.Int).Exp(i.e, reducedS, i.params.p)})
}

func (i IntegerElement) Negate() group.Element {
	return group.Element(IntegerElement{params: i.params, e: new(big.Int).Neg(i.e)})
}
